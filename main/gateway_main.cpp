#include <vector>
#include <iostream>
#include <thread>
#include <signal.h>
#include "gateway/gateway.h"

using std::vector;
using std::thread;
using Exchange::Gateway;

void sigint_handler(int sig) {
  std::cout << "SIGINT received" << std::endl;
  exit(0);
}

int main() {
  {
    // Register signal and signal handler
    struct sigaction sigIntHandler;
    sigIntHandler.sa_handler = sigint_handler;
    sigemptyset(&sigIntHandler.sa_mask);
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);
  }

  Gateway gateway("../main/config.toml");

  vector<thread> threads;
  threads.push_back(thread(&Gateway::parse_incoming_from_mpid_messages, &gateway));
  threads.push_back(thread(&Gateway::parse_incoming_from_ome_messages, &gateway));

  for (auto& t : threads) {
    t.join();
  }
}