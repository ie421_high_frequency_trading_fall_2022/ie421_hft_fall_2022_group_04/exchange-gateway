#include <toml.hpp>
#include <iostream>
#include "gateway/gateway.h"

using toml::find;
using toml::parse;
using Exchange::Gateway;

int main() {
  Gateway gateway("../main/config.toml");
  
  std::cout << "Gateway name: " << gateway.get_name() << std::endl;
  std::cout << "Gateway ip: " << gateway.get_ip() << std::endl;
  std::cout << "Number of OMEs: " << gateway.get_num_omes() << std::endl;
  std::cout << "Number of participants: " << gateway.get_num_participants() << std::endl;

  Gateway g = gateway;
}