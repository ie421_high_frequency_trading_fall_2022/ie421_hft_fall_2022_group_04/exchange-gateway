#pragma once
#include <rigtorp/SPSCQueue.h>
#include <vector>
#include <string>
#include <unordered_map>
#include "gateway/structs.h"

#define DEFAULT_QUEUE_SIZE 512
#define BUFFER_SIZE 128

template <typename K, typename V>
using map = std::unordered_map<K, V>;
using std::vector;
using rigtorp::SPSCQueue;

namespace Exchange {
  class Gateway {
    public:
      Gateway(std::string config_filepath);
      Gateway(const Gateway& other);
      Gateway& operator=(const Gateway& other);
      ~Gateway();

      // Multithreading functions
      void parse_incoming_from_mpid_messages();
      void parse_incoming_from_ome_messages();
      
      void mpid_to_ome_logic(unsigned char* message, Participant_struct participant);
      void ome_to_mpid_logic(unsigned char* message);
      // void start();
      // void stop();

      // Accessors
      std::string get_name() const { return name_; };
      std::string get_ip() const { return ip_; };
      size_t get_num_omes() const { return omes_.size(); };
      size_t get_num_participants() const { return market_participants_.size(); };
      vector<OME_struct> get_omes() const { return omes_; };
      vector<Participant_struct> get_participants() const { return market_participants_; };

    private:
      std::string config_filepath_;
      std::string name_;
      std::string ip_;

      vector<OME_struct> omes_;
      vector<Participant_struct> market_participants_;

      vector<SPSCQueue<unsigned char*>*> inbound_from_mpid_queue_; // one for each mpid
      vector<SPSCQueue<unsigned char*>*> outbound_to_ome_queue_; // one for each ome
      vector<SPSCQueue<unsigned char*>*> inbound_from_ome_queue_; // one for each ome
      vector<SPSCQueue<unsigned char*>*> outbound_to_mpid_queue_; // one for each mpid
      
      // vector<tcp_server> // one for each mpid
      // vector<tcp_client> // one for each ome

      map<std::string, size_t> symbol_to_ome_map_;
      map<std::string, size_t> ome_name_to_index_map_;
      map<std::string, size_t> mpid_name_to_index_map_;

      // Only map that is build as we go
      map<std::string, std::string> orderID_to_ome_name_map_; // TODO: Remember to delete?
      map<std::string, std::string> orderID_to_mpid_name_map_; // TODO: Remember to delete?

      void _parse_config();
      void _instantiate_queues();
      void _build_symbol_to_ome_map();
      void _build_ome_name_to_index_map();
      void _build_mpid_name_to_index_map();

      void _copy(const Gateway& other);
      void _delete();

      // void _send_tcp_packet(tcp_server server, unsigned char* message);
      // unsigned char* _recv_tcp_packet(tcp_client client);
  };
}