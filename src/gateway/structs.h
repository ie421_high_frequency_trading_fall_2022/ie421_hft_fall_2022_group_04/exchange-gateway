#pragma once
#include <string>
#include <vector>

#define MPID_LEN 4
#define ORDER_TOKEN_LEN 14
#define SYMBOL_LEN 8

typedef struct OME_struct {
  std::string name;
  std::string ipv4;
  std::vector<std::string> symbols;
} OME_struct;

typedef struct Participant_struct {
  std::string name;
  std::string location;
  std::string ipv4;
  uint64_t port;
  std::string mpid;
} Participant_struct;