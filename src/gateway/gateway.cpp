#include "gateway/gateway.h"
#include <toml.hpp>
#include <iostream>
#include <thread>

using rigtorp::SPSCQueue;

namespace Exchange {

  Gateway::Gateway(std::string config_filepath) : config_filepath_(config_filepath) {
    _parse_config();
    _instantiate_queues();
    _build_symbol_to_ome_map();
    _build_ome_name_to_index_map();
    _build_mpid_name_to_index_map();
    assert(omes_.size() == inbound_from_ome_queue_.size());
    assert(omes_.size() == outbound_to_ome_queue_.size());
    assert(market_participants_.size() == inbound_from_mpid_queue_.size());
    assert(market_participants_.size() == outbound_to_mpid_queue_.size());
  }

  Gateway::Gateway(const Gateway& other) {
    _copy(other);
  }

  Gateway& Gateway::operator=(const Gateway& other) {
    if (this != &other) {
      _delete();
      _copy(other);
    }
    return *this;
  }

  Gateway::~Gateway() {
    _delete();
  }

  void Gateway::parse_incoming_from_mpid_messages() {
    size_t num_participants = get_num_participants();
    while(true) {
      for (size_t i = 0; i < num_participants; i++) {
        if (!inbound_from_mpid_queue_[i]->empty()) {
          unsigned char* message = *inbound_from_mpid_queue_[i]->front();
          mpid_to_ome_logic(message, market_participants_[i]);
          inbound_from_mpid_queue_[i]->pop();
        }
      }
      std::this_thread::yield();
    }
  }

  void Gateway::parse_incoming_from_ome_messages() {
    size_t num_omes = get_num_omes();
    while(true) {
      for (size_t i = 0; i < num_omes; i++) {
        if (!inbound_from_ome_queue_[i]->empty()) {
          unsigned char* message = *inbound_from_ome_queue_[i]->front();
          ome_to_mpid_logic(message);
          inbound_from_ome_queue_[i]->pop();
        }
      }
      std::this_thread::yield();
    }
  }

  void Gateway::mpid_to_ome_logic(unsigned char* message, Participant_struct participant) {
    unsigned char buffer[BUFFER_SIZE];
    memset(buffer, 0, BUFFER_SIZE);
    memcpy(buffer, message, BUFFER_SIZE);
    
    char type = buffer[0];
    switch (type) {
      case 'O':
        // Order Entry
        {
          char symbol_arr[SYMBOL_LEN];
          memcpy(symbol_arr, buffer + 20, SYMBOL_LEN);
          std::string symbol(symbol_arr, SYMBOL_LEN);

          size_t ome_index = symbol_to_ome_map_[symbol];
          OME_struct ome = omes_[ome_index];

          char orderID_arr[ORDER_TOKEN_LEN];
          memcpy(orderID_arr, buffer + 1, ORDER_TOKEN_LEN);
          std::string orderID(orderID_arr, ORDER_TOKEN_LEN);

          orderID_to_ome_name_map_[orderID] = ome.name;
          orderID_to_mpid_name_map_[orderID] = participant.name;
          // TODO: Send TCP packet to ome
        }
        break;
      case 'U':
        // Order Replace
        // TODO: Implement
        std::cout << "Gateway: Order Replace" << std::endl;
        break;
      case 'X': 
        // Order Cancel
        {
          memcpy(buffer + 28, participant.mpid.c_str(), MPID_LEN);
          char orderID_arr[ORDER_TOKEN_LEN];
          memcpy(orderID_arr, buffer + 15, ORDER_TOKEN_LEN);
          std::string orderID(orderID_arr, ORDER_TOKEN_LEN);
          std::string ome_name = orderID_to_ome_name_map_[orderID];
          size_t ome_index = ome_name_to_index_map_[ome_name];
          OME_struct ome = omes_[ome_index];
          // TODO: Send TCP packet to ome
        }
        break;
      case 'M':
        // Order Modify
        // TODO: Implement
        std::cout << "Gateway: Order Modify" << std::endl;
        break;
      default:
        // TODO: Log error
        break;
    }
  }

  void Gateway::ome_to_mpid_logic(unsigned char* message) {
    unsigned char buffer[BUFFER_SIZE];
    memset(buffer, 0, BUFFER_SIZE);
    memcpy(buffer, message, BUFFER_SIZE);

    char type = buffer[0];
    switch (type) {
      case 'A':
        // Order Accepted <-
        {
          char mpid_arr[MPID_LEN];
          memcpy(mpid_arr, buffer + 44, MPID_LEN);
          std::string mpid_name(mpid_arr, MPID_LEN);
          
          size_t mpid_index = mpid_name_to_index_map_[mpid_name];
          Participant_struct participant = market_participants_[mpid_index];
          // TODO: Send TCP packet to mpid
        }
        break;
      case 'U':
        // Order Replaced
        // TODO: Implement
        std::cout << "Gateway: Order Replaced" << std::endl;
        break;
      case 'C':
        // Order Cancelled <-
        {
          char mpid_arr[MPID_LEN];
          memcpy(mpid_arr, buffer + 28, MPID_LEN);
          std::string mpid_name(mpid_arr, MPID_LEN);

          size_t mpid_index = mpid_name_to_index_map_[mpid_name];
          Participant_struct participant = market_participants_[mpid_index];
          // TODO: Send TCP packet to mpid
        }
        break;
      case 'E':
        // Order Executed <-
        {
          char mpid_arr[MPID_LEN];
          memcpy(mpid_arr, buffer + 40, MPID_LEN);
          std::string mpid_name(mpid_arr, MPID_LEN);

          size_t mpid_index = mpid_name_to_index_map_[mpid_name];
          Participant_struct participant = market_participants_[mpid_index];
          // TODO: Send TCP packet to mpid
        }
        break;
      case 'J':
        // Order Rejected
        // TODO: Implement
        std::cout << "Gateway: Order Rejected" << std::endl;
        break;
      case 'I':
        // Cancel Rejected
        // TODO: Implement
        std::cout << "Gateway: Cancel Rejected" << std::endl;
        break;
      case 'T':
        // Order Priority Update
        // TODO: Implement
        std::cout << "Gateway: Order Priority Update" << std::endl;
        break;
      default:
        break;
    }
  }

  void Gateway::_parse_config() {
    toml::value data;
    try {
      data = toml::parse(config_filepath_);
    } catch (...) {
      std::cout << "Could not find config file" << std::endl;
      exit(1);
    }

    const auto& startup_config = toml::find(data, "startup-config");
    const auto& order_matching_engine = toml::find(data, "order-matching-engine");
    const auto& market_parti = toml::find(data, "market-participants");

    // Populate class member variables
    name_ = toml::find<std::string>(startup_config, "name");
    ip_ = toml::find<std::string>(startup_config, "ipv4");

    for (auto& ome : order_matching_engine.as_array()) {
      OME_struct ome_struct;
      ome_struct.name = toml::find<std::string>(ome, "name");
      ome_struct.ipv4 = toml::find<std::string>(ome, "ome_ipv4");
      for (auto& symbol : toml::find(ome, "ome_symbols").as_array()) {
        ome_struct.symbols.push_back(symbol.as_string());
      }
      omes_.push_back(ome_struct);
    }
    
    for (auto& participant : market_parti.as_array()) {
      Participant_struct participant_struct;
      participant_struct.name = toml::find<std::string>(participant, "name");
      participant_struct.location = toml::find<std::string>(participant, "location");
      participant_struct.ipv4 = toml::find<std::string>(participant, "ipv4");
      participant_struct.port = toml::find<uint64_t>(participant, "port");
      participant_struct.mpid = toml::find<std::string>(participant, "mpid");
      market_participants_.push_back(participant_struct);
    }

  }

  void Gateway::_instantiate_queues() {
    size_t num_participants = get_num_participants();
    for (size_t i = 0; i < num_participants; i++) {
      inbound_from_mpid_queue_.push_back(new SPSCQueue<unsigned char*>(DEFAULT_QUEUE_SIZE));
      outbound_to_mpid_queue_.push_back(new SPSCQueue<unsigned char*>(DEFAULT_QUEUE_SIZE));
    }

    size_t num_omes = get_num_omes();
    for (size_t i = 0; i < num_omes; i++) {
      outbound_to_ome_queue_.push_back(new SPSCQueue<unsigned char*>(DEFAULT_QUEUE_SIZE));
      inbound_from_ome_queue_.push_back(new SPSCQueue<unsigned char*>(DEFAULT_QUEUE_SIZE));
    }
  }

  void Gateway::_build_symbol_to_ome_map() {
    for (size_t i = 0; i < omes_.size(); i++) {
      for (size_t j = 0; j < omes_[i].symbols.size(); j++) {
        symbol_to_ome_map_[omes_[i].symbols[j]] = i;
      }
    }
  }

  void Gateway::_build_ome_name_to_index_map() {
    for (size_t i = 0; i < omes_.size(); i++) {
      ome_name_to_index_map_[omes_[i].name] = i;
    }
  }

  void Gateway::_build_mpid_name_to_index_map() {
    for (size_t i = 0; i < market_participants_.size(); i++) {
      mpid_name_to_index_map_[market_participants_[i].name] = i;
    }
  }


  void Gateway::_copy(const Gateway& other) {
    config_filepath_ = other.config_filepath_;
    name_ = other.name_;
    ip_ = other.ip_;
    omes_ = other.omes_;
    market_participants_ = other.market_participants_;

    // Deep copy spsc queue
    for (size_t i = 0; i < other.inbound_from_mpid_queue_.size(); i++) {
      inbound_from_mpid_queue_.push_back(new SPSCQueue<unsigned char*>(DEFAULT_QUEUE_SIZE));
    }
    for (size_t i = 0; i < other.outbound_to_ome_queue_.size(); i++) {
      outbound_to_ome_queue_.push_back(new SPSCQueue<unsigned char*>(DEFAULT_QUEUE_SIZE));
    }
    for (size_t i = 0; i < other.inbound_from_ome_queue_.size(); i++) {
      inbound_from_ome_queue_.push_back(new SPSCQueue<unsigned char*>(DEFAULT_QUEUE_SIZE));
    }
    for (size_t i = 0; i < other.outbound_to_mpid_queue_.size(); i++) {
      outbound_to_mpid_queue_.push_back(new SPSCQueue<unsigned char*>(DEFAULT_QUEUE_SIZE));
    }

    symbol_to_ome_map_ = other.symbol_to_ome_map_;
    ome_name_to_index_map_ = other.ome_name_to_index_map_;
    mpid_name_to_index_map_ = other.mpid_name_to_index_map_;
    orderID_to_ome_name_map_ = other.orderID_to_ome_name_map_;
    orderID_to_mpid_name_map_ = other.orderID_to_mpid_name_map_;
  }

  void Gateway::_delete() {
    // Set spsc queue to nullptr
    for (size_t i = 0; i < inbound_from_mpid_queue_.size(); i++) {
      delete inbound_from_mpid_queue_[i];
    }
    inbound_from_mpid_queue_.clear();
    
    for (size_t i = 0; i < outbound_to_ome_queue_.size(); i++) {
      delete outbound_to_ome_queue_[i];
    }
    outbound_to_ome_queue_.clear();

    for (size_t i = 0; i < inbound_from_ome_queue_.size(); i++) {
      delete inbound_from_ome_queue_[i];
    }
    inbound_from_ome_queue_.clear();

    for (size_t i = 0; i < outbound_to_mpid_queue_.size(); i++) {
      delete outbound_to_mpid_queue_[i];
    }
    outbound_to_mpid_queue_.clear();
  }
}